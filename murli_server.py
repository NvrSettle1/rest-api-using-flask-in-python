from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps


db_connect = create_engine('sqlite:///murli.db')
app = Flask(__name__)
api = Api(app)


class Murli(Resource):
    def get(self, murli_id):
        conn = db_connect.connect()
        query = conn.execute("select * from murli where ID =%d "  %int(murli_id))
        result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        return jsonify(result)


class Murlis(Resource):
    def get(self):
        conn = db_connect.connect()
        query = conn.execute("select ID, title from murli;")
        result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        return jsonify(result)

api.add_resource(Murli, '/murli/<murli_id>') # Route_1
api.add_resource(Murlis, '/murlis') # Route_2


if __name__ == '__main__':
     app.run(port=5002)
